import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by asis on 3/10/17.
 */
public class ProcessSend {

    private String sourceFileName;
    private String destFileName;
    private String ip;
    private int port;

    private int numberOfPackets;

    public ProcessSend(String sourceFileName, String destFileName, String ip, int port) {
        this.sourceFileName = sourceFileName;
        this.destFileName = destFileName;
        this.ip = ip;
        this.port = port;
    }

    public void startSendingFile()
        throws Exception
    {
        numberOfPackets = FileSpliter.splitFile(new File(sourceFileName));
        System.out.println("File is loaded " + numberOfPackets + " are to be send");

        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName(ip);

        byte[] receivedData = new byte[1024];

//        String strFirstDataToBeSend = destFileName + " " + numberOfPackets;

        FileObject fileObject = new FileObject(destFileName, String.valueOf(numberOfPackets));

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(outputStream);
        oos.writeObject(fileObject);

        byte[] sendData = outputStream.toByteArray();

        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
        clientSocket.send(sendPacket);

        DatagramPacket receivePacket = new DatagramPacket(receivedData, receivedData.length);
        clientSocket.receive(receivePacket);
        byte[] data = receivePacket.getData();

        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        FileObject receivedfileObject = (FileObject) is.readObject();

        if(receivedfileObject.getIsAckSignal()){
            System.out.println(receivedfileObject.getMessage());
        }

        clientSocket.close();
    }

    public void sendFilePackets(){
        List<String> strFilesToTransfer = getFilesName();

        try {
            DatagramSocket socket = new DatagramSocket();
            InetAddress ipAddress = InetAddress.getByName(ip);

            int sequenceNumber = 0;

            for (String filename : strFilesToTransfer) {
                File file = new File(Constants.HOME_PATH+Constants.TEMP_FOLDER+"/"+filename);
                String sequence = String .valueOf(sequenceNumber);



                FileObject fileObject = getFileObject(sequence,file);
                sequenceNumber++;

                System.out.println("Sending Packet " + sequenceNumber + " of " + (numberOfPackets+1) );

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(outputStream);
                oos.writeObject(fileObject);

                byte[] dataSend = outputStream.toByteArray();

                DatagramPacket sendPacket = new DatagramPacket(dataSend, dataSend.length, ipAddress, port);
                socket.send(sendPacket);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void reSendPackets(String msg){

        String[] fileName = msg.split(":");


        try {
            DatagramSocket socket = new DatagramSocket();
            InetAddress ipAddress = InetAddress.getByName(ip);


            for (int i = 0; i < fileName.length ; i++) {
                if(!fileName[i].equals("")) {

                    System.out.println("Filename " + Constants.HOME_PATH + Constants.TEMP_FOLDER + "/" + "1.mp4" + "." + String.format("%03d", Integer.valueOf(fileName[i])));

                    File file = new File(Constants.HOME_PATH + Constants.TEMP_FOLDER + "/" + "500.mkv" + "." + String.format("%03d", Integer.valueOf(fileName[i])));

                    FileObject fileObject = getFileObject(fileName[i], file);

                    System.out.println("Sending Packet " + fileName[i] + " of " + (numberOfPackets + 1));

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(outputStream);
                    oos.writeObject(fileObject);

                    byte[] dataSend = outputStream.toByteArray();

                    DatagramPacket sendPacket = new DatagramPacket(dataSend, dataSend.length, ipAddress, port);
                    socket.send(sendPacket);
                }
            }
            socket.close();
            askAckSignal();
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public void askAckSignal()
            throws Exception
    {

        System.out.println("File Sending is Completed. Calculating the missing packets");

        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName(ip);

        byte[] receivedData = new byte[1024];


        FileObject fileObject = new FileObject(true,Constants.ACK_PACKET_COUNT);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(outputStream);
        oos.writeObject(fileObject);

        byte[] sendData = outputStream.toByteArray();

        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
        clientSocket.send(sendPacket);

        DatagramPacket receivePacket = new DatagramPacket(receivedData, receivedData.length);
        clientSocket.receive(receivePacket);
        byte[] data = receivePacket.getData();

        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        FileObject receivedfileObject = (FileObject) is.readObject();

        if(receivedfileObject.getIsAckSignal()){
            if(!receivedfileObject.getMessage().equals(Constants.ACK_COMPLETED)){
                reSendPackets(receivedfileObject.getMessage());
            }
        }
        clientSocket.close();
    }


    private FileObject getFileObject(String sequence, File file){

        FileObject fileObject = null;

        if (file.isFile()) {
            try {
                DataInputStream diStream = new DataInputStream(new FileInputStream(file));
                long len = (int) file.length();
                byte[] fileBytes = new byte[(int) len];
                int read = 0;
                int numRead = 0;
                while (read < fileBytes.length && (numRead = diStream.read(fileBytes, read, fileBytes.length - read)) >= 0) {
                    read = read + numRead;
                }

                fileObject = new FileObject(sequence, fileBytes);

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }

        return fileObject;
    }

    private List<String> getFilesName(){
        File folder = new File(Constants.HOME_PATH + Constants.TEMP_FOLDER);
        File[] listOfFiles = folder.listFiles();
        List<String> filesName = new ArrayList<>();

        for (File file : listOfFiles) {
            if (file.isFile()) {
//                System.out.println("Names " + file.getName());
                filesName.add(file.getName());
            }
        }

        return filesName;
    }

}
