import java.io.File;
import java.io.Serializable;

/**
 * Created by asis on 2/21/17.
 */
public class FileObject implements Serializable {

    private String fileName;
    private String totalPackets;


    private String sequenceNumber;
    private byte[] fileData;

    private boolean isFirstPacket;
    private boolean isAckSignal;
    private boolean isAfile;

    private String message;

    public FileObject(String fileName,String totalPackets){
        this.fileName = fileName;
        this.totalPackets = totalPackets;
        isFirstPacket = true;

    }

    public FileObject(String sequenceNumber, byte[] actualFile) {
        this.sequenceNumber = sequenceNumber;
        this.fileData = actualFile;
        isFirstPacket = false;
        isAfile = true;
    }

    public FileObject(boolean isAckSignal,String message){
        this.isAckSignal = isAckSignal;
        this.message = message;
    }

    public boolean isAFile(){
        return isAfile;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTotalPackets() {
        return totalPackets;
    }

    public void setTotalPackets(String totalPackets) {
        this.totalPackets = totalPackets;
    }

    public String getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public byte[] getActualFile() {
        return fileData;
    }

    public void setActualFile(byte[] fileData) {
        this.fileData = fileData;
    }

    public String getMessage(){
        return message;
    }

    public  boolean getIsFirstPacket(){
        return isFirstPacket;
    }

    public boolean getIsAckSignal(){
        return isAckSignal;
    }
}
