import java.io.File;
import java.io.IOException;
import java.net.DatagramSocket;

public class Netrcv{

	public static void main(String[] args){

		if(args.length == 1 && args[0].equals("format")){
			System.out.println("Format: <comp_port>");
			System.exit(0);
		}

		if(args.length != 1){
			System.out.println("Error on input format!!");
			System.out.println("Format: <comp_port>");
			System.exit(1);
		}
		int compPort = 8000; //default port
		try{
			compPort = Integer.parseInt(args[0]);
			System.out.println("Server is running at port " + compPort + " ready to accept files" );
		}catch(NumberFormatException ex){
			System.out.println("Parameter <comp_port> should be a number");
			System.exit(2);
		}catch (Exception ex){
			System.out.println("Something went wrong");
			ex.printStackTrace();
			System.exit(3);
		}

		try{
			ProcessReceived received = new ProcessReceived();
			received.startRecevingFile(compPort);

			System.out.println( "Receving Data is Completed and now the downloaded files are merging" );
			FileSpliter.mergeFiles(Constants.HOME_PATH + Constants.DOWNLOAD_TEMP, Constants.HOME_PATH + Constants.DOWNLOAD_FILE + received.getFileName());
		}catch (Exception ex){
			ex.printStackTrace();
		}
	}
}