import java.io.*;
import java.net.*;


/*
 TCP works by breaking down the data into smaller chunks that fit inside individual IP packets.
 Since the IP packets might be reordered en route and some might not appear at all,
 each packet has a sequence number that allows the recipient to reassemble them into the right order.
 The recipient must also acknowledge each received packet; if the sender doesn'tmp receive an
 acknowledgement of packet x within a certain amount of time, it will resent packet x
 (and keep doing so at regular intervals until it is acknowledged).
 This means that the recipient must also be prepared to receive multiple copies of the same packet.

  So in your situation, you need to decide upon some format for your UDP datagrams:
  for example, the first eight bytes might contain a random identifier that the sender
  chooses for the file (so that different files can be transferred simultaneously without the
  datagrams getting mixed up), the next four bytes will be the sequence number, and the rest
  of the packet will be a chunk of the actual file data. Maybe the first packet should contain
  the filename as well, and the total number of datagrams the file consists of. An acknowledgement
  datagram need only contain the identifier and the sequence number.
*/

public class Netcpy{

	private static String sourceFileName;
	private static String destFileName;
	private static String compIp;
	private static int compPort;

	public static void main(String[] args){
		
		if(args.length == 1 && args[0].equals("format")){
			System.out.println("Format: <source_file_name> <dest_file_name> <comp_ip> <comp_port>");
			System.exit(0);
		}

		if(args.length != 4){
			System.out.println("Error on input format!!");
			System.out.println("Format: <source_file_name> <dest_file_name> <comp_ip> <comp_port>");
			System.exit(1);
		}

		sourceFileName = args[0];
		destFileName = args[1];

		compIp = args[2];
		//compPort = 8000; //default port
		
		try{
			compPort = Integer.parseInt(args[3]);
		}catch(NumberFormatException ex){
			System.out.println("Parameter <comp_port> should be a number");
			System.exit(2);
		}

		ProcessSend send = null;

		try{
			send = new ProcessSend(sourceFileName,destFileName,compIp,compPort);
			System.out.println("File Transfer has started");
			send.startSendingFile();
			System.out.println("server is ready to capture the packets");

			send.sendFilePackets();

			send.askAckSignal();

		}catch (Exception ex){
			System.out.println("Something went wrong");
			ex.printStackTrace();
			System.exit(3);
		}
	}
}