/**
 * Created by asis on 3/10/17.
 */
public class Constants {

    public static String HOME_PATH = "/Users/asis/workspace/Class/DS/src/files";

    public static String TEMP_FOLDER = "/tmp";

    public static String DOWNLOAD_TEMP = "/downloads/tmp";

    public static String DOWNLOAD_FILE = "/downloads/";


    public static String ACK_COMPLETED = "completed";
    public static String ACK_PACKET_COUNT = "packetcount";

}
