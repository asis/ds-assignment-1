import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by asis on 3/10/17.
 */
public class ProcessReceived {

    private String fileName;
    private int packets;
    private int packetsReceived;
    private int[] successPackets;

    public ProcessReceived(){
        packetsReceived = 0;
    }

    public String getFileName(){
        return fileName;
    }

    public void startRecevingFile(int p)
        throws Exception
    {
        DatagramSocket serverSocket = new DatagramSocket(p);
        byte[] receiveData = new byte[1024 * 1000 * 50];

        boolean status = true;

        while(status)
        {
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            serverSocket.receive(receivePacket);
            byte[] data = receivePacket.getData();

            ByteArrayInputStream in = new ByteArrayInputStream(data);
            ObjectInputStream is = new ObjectInputStream(in);
            FileObject fileObject = (FileObject) is.readObject();

            if(fileObject != null) {

                //for first packet
                if (fileObject.getIsFirstPacket()) {
                    fileName = fileObject.getFileName();
                    packets = Integer.valueOf(fileObject.getTotalPackets());

                    successPackets = new int[packets + 1];

                    InetAddress IPAddress = receivePacket.getAddress();
                    int port = receivePacket.getPort();
                    //String sendSentence = "Information received";
                    FileObject sendFileObject = new FileObject(true, "Information Received");
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(outputStream);
                    oos.writeObject(sendFileObject);

                    byte[] sendData = outputStream.toByteArray();

                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                    serverSocket.send(sendPacket);

                    // Acknowledgement
                } else if (fileObject.getIsAckSignal()) {
                    String message = fileObject.getMessage();
                    System.out.println(message);

                    InetAddress IPAddress = receivePacket.getAddress();
                    int port = receivePacket.getPort();

                    FileObject sendFileObject;

                    System.out.println("received packets " + packetsReceived + " total packets " + packets);

                    if (packetsReceived == packets) {
                        sendFileObject = new FileObject(true, "completed");
                        status = false;
                    } else {
                        String missingPackets = getFailurePackets();
                        sendFileObject = new FileObject(true, missingPackets);
                    }

                    //String sendSentence = "Information received";

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(outputStream);
                    oos.writeObject(sendFileObject);

                    byte[] sendData = outputStream.toByteArray();

                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
                    serverSocket.send(sendPacket);

                    //Actual file data
                } else if (fileObject.isAFile()) {
                    String sequqnceNumber = fileObject.getSequenceNumber();
                    byte[] filePacket = fileObject.getActualFile();
                    storeThePackets(sequqnceNumber, filePacket);
                }
            }
        }
    }

    private void storeThePackets(String sequenceNumber, byte[] file){
        packetsReceived++;

        successPackets[Integer.valueOf(sequenceNumber)] = 1;

        String outputFile = Constants.HOME_PATH + Constants.DOWNLOAD_TEMP + "/"+ sequenceNumber;
        if (!new File(Constants.HOME_PATH + Constants.DOWNLOAD_TEMP).exists()) {
            new File(Constants.HOME_PATH + Constants.DOWNLOAD_TEMP).mkdirs();
        }
        File dstFile = new File(outputFile);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(dstFile);
            fileOutputStream.write(file);
            fileOutputStream.flush();
            fileOutputStream.close();
            System.out.println("Output file : " + outputFile + " is successfully saved ");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFailurePackets(){
        String data = "";
        for (int i = 0; i < packets; i++){
            if (successPackets[i] == 0){
                data += i + ":";
            }
        }
        System.out.println("Failure packets" + data);
        return data;
    }
}
