import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by asis on 3/9/17.
 */

// FileSpliter.splitFile(new File("/Users/asis/workspace/Class/DS Project/src/files/1.mp4"));

// FileSpliter.mergeFiles("/Users/asis/workspace/Class/DS Project/src/files/tmp/1.mp4.000","/Users/asis/workspace/Class/DS Project/src/files/original.mp4");

public class FileSpliter {

    public static int splitFile(File f){
        int partCounter = 0;

        int sizeOfFiles = 1000 * 50;  // 1KB * 50 = 50KB
        byte[] buffer = new byte[sizeOfFiles];

        try (BufferedInputStream bis = new BufferedInputStream( new FileInputStream(f))) {

            String name = f.getName();

            createDirectory(f);

            int tmp = 0;
            while ((tmp = bis.read(buffer)) > 0) {
                File newFile = new File(f.getParent(), "/tmp/" + name + "." + String.format("%03d", partCounter++));

                try (FileOutputStream out = new FileOutputStream(newFile)) {
                    out.write(buffer, 0, tmp);
                }
            }
        }catch (FileNotFoundException e){   e.printStackTrace();}
        catch (IOException e){  e.printStackTrace();}

        return partCounter;
    }

    private static void createDirectory(File f){
        File theDir = new File(f.getParent() + "/tmp");

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            boolean result = false;

            try{
                theDir.mkdir();
                result = true;
            }
            catch(SecurityException se){
                //handle it
            }
            if(result) {
                System.out.println("DIR created");
            }
        }
    }

    private static void mergeFiles(List<File> files, File into)
            throws IOException {
        try (BufferedOutputStream mergingStream = new BufferedOutputStream(
                new FileOutputStream(into))) {
            for (File f : files) {
                Files.copy(f.toPath(), mergingStream);
            }
        }
    }


    public static void mergeFiles(String path, String into) throws IOException{
        mergeFiles(listOfFilesToMerge(path), new File(into));
    }

    private static List<File> listOfFilesToMerge(String path){
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        System.out.println("number of file " + listOfFiles.length);
        List<File> list = new ArrayList<>();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                list.add(file);
            }
        }
        System.out.println("Size of list " + list.size());
        return list;
    }

    public static int numberOfFiles(String path){
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();
        return listOfFiles.length;
    }

}
